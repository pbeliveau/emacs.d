(use-package spotify
  :ensure t
  :config (spotify-enable-song-notifications))
