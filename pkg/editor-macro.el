(use-package elmacro
  :ensure t
  :diminish elmacro
  :bind (("C-c C-e" . elmacro-mode)
         ("C-x C-)" . elmacro-show-last-macro)))
