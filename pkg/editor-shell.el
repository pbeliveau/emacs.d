(use-package better-shell
    :ensure t
    :bind (("C-c ;" . better-shell-shell)
           ("C-c '" . better-shell-remote-open)))
